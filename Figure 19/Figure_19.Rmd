---
title: "Figure_19 efm hsd and host"
output: html_document
---

```{r}
#load packages 
library(ggtreeExtra)
library(ggstar)
library(ggplot2)
library(ggtree)
library(treeio)
library(ggnewscale)
library(reshape2)
```

#load data
```{r}
tree2 <- read.tree("/Users/georgiacampbell/Desktop/efm/mafft_efm_red_out.treefile")
efm_host_reshaped <- read_csv("Dropbox/My Mac (Georgia’s MacBook Pro)/Desktop/code to upload/Fig19/efm_host_reshaped.csv")
efm_df_per2 <- read_csv("Dropbox/My Mac (Georgia’s MacBook Pro)/Desktop/code to upload/Fig19/efm_df_per2.csv")
```


```{r}
#plot tree
e <- ggtree(tree) + 
    geom_tiplab(size=4, align=TRUE, linesize=.5)
e
```
#plot hsd genes against MLST tree 
```{r}
#reshape data to fit tree 
efm_df_per2 <- melt(efm_df_per2, id=c("ST_TYPE"))
colnames(efm_df_per2)[1] <- "y"
colnames(efm_df_per2)[3] <-  "Percent_hsd_genes_present"


#to add hsd gene labels 
efm_df_per2$xindex <- as.numeric(efm_df_per2$variable)
per_df_txt <-efm_df_per2[!duplicated(efm_df_per2$xindex ), ]
  
p1 <- facet_plot(e, panel = "Percentage of hsd genes present", data = efm_df_per2,  geom=geom_tile, mapping = aes(x=xindex, y=y, fill=Percent_hsd_genes_present)) + scale_fill_continuous(low="grey", high="blue")
p1 <- p1 + xlim_tree(max(p1$data$x)+0.7*max(p1$data$x))

facet_labeller(p1, c(Tree= "MLST Tree")) 
p1


```

#plot host data against MLST tree 
```{r}
colnames(efm_host_reshaped)[1] <- "y"
colnames(efm_host_reshaped)[3] <-"Source"

#index so can plot discrete variables with facet plot 
efm_host_reshaped$Source <- factor(efm_host_reshaped$Source,
levels=unique(efm_host_reshaped$Source))
efm_host_reshaped$xindex <- as.numeric(efm_host_reshaped$Source)
df_txt <-efm_host_reshaped[!duplicated(efm_host_reshaped$xindex), ]


p2 <- facet_plot(e, panel = "Isolation source", data = efm_host_reshaped,  geom=geom_tile, mapping = aes(x=xindex, y=y, fill=Source)) +  scale_fill_manual(values = c("Poultry" = "blue", "Human" = "dark red", "Bovine"= "dark grey", "Swine" = "orange"))


p2 <- p2 + xlim_tree(max(p2$data$x)+0.5*max(p2$data$x))
facet_labeller(p1, c(Tree= "MLST Tree")) 
p2
```


