---
title: "efm_chi-square_virulence"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
#load data 
efm_X2_virulence <- read_csv("~/Dropbox/My Mac (Georgia’s MacBook Pro)/Desktop/efm_x2/efm_X2_virulence.csv")
```

```{r}
total <- data.frame(x1 = (c(1284, 862)))
rownames(total) <- c("type_I_RM", "no_type_I_RM")  
```
#get p-value for virulence genes 

```{r}
#ace 
ace <- efm_X2_virulence[,1:2]
ace$no_ace <- (total$x1 - ace$ace)
ace <- ace[-c(1:1)]
a <- chisq.test(ace)
a #X-squared = 2.3175e-26, df = 1, p-value = 1

#acm
efm_X2_virulence$acm
acm <- efm_X2_virulence[,1:3]
acm <- acm[-c(1:2)]
acm$no_acm <- (total$x1 -acm$acm)
b <- chisq.test(acm)
b # X-squared = 9.4097, df = 1, p-value = 0.002158

b$observed
b$expected

#ecbA 
ecbA <- efm_X2_virulence[,1:4]
ecbA <- ecbA[-c(1:3)]
ecbA$no_ecbA <- (total$x1- ecbA$ecbA)
c <- chisq.test(ecbA)
c #X-squared = 4.5558, df = 1, p-value = 0.03281
c$observed
c$expected
efm_X2_virulence$esp
#esp
esp <- efm_X2_virulence[,1:5]
esp <- esp[-c(1:4)]
esp$no_esp <- (total$x1- esp$esp)
d <- chisq.test(esp)
d #X-squared = 0.11164, df = 1, p-value = 0.7383

#fss3
fss3 <-  efm_X2_virulence[,1:6]
fss3 <- fss3[-c(1:5)]
fss3$no_fss3 <- (total$x1 - fss3$fss3)
e <- chisq.test(fss3)
e
e$observed
e$expected
#X-squared = 42.42, df = 6, p-value = 1.519e-07

#scm
scm <- efm_X2_virulence[,1:7]
scm <- scm[-c(1:6)]
scm$no_scm <- (total$x1 - scm$scm)
f <- chisq.test(scm)
f #X-squared = 24.206, df = 1, p-value = 8.655e-07
f$observed
f$expected
#sgrA
sgrA <- efm_X2_virulence[,1:8]
sgrA <- sgrA[-c(1:7)]
sgrA$no_sgrA <- (total$x1 - sgrA$sgrA )
g <- chisq.test(sgrA)
g #X-squared = 117.37, df = 1, p-value < 2.2e-16
g$observed
g$expected
```
